﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClientOnline.Model
{
    public class Cliente
    {
        [Key]
        public int Id { get; set; }

        [Display(Name="Cliente")]
        [Required(ErrorMessage ="O nome do {0} é obrigatório.")]
        public string Nome { get; set; }

        [Display(Name ="Gênero")]
        [Required(ErrorMessage = "O gênero {0} é obrigatório.")]
        public bool Sexo { get; set; }

    }
}