﻿using ClientOnline.Repositorio.Infra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientOnline.Repositorio
{
    public class Repositorio<T> : IDisposable where T : class
    {

        public Repositorio()
        {
            contexto = new Contexto();
        }

        public void Dispose()
        {
            contexto.Dispose();
            contexto = null;
        }


        protected Contexto contexto { get; set; }

        public List<T> Listar()
        {
            try
            {
                return contexto.Set<T>().ToList();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

        //public T Inserir(T entitidade)
        //{

        //}

    }
}