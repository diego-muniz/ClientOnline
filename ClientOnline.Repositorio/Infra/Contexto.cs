﻿using ClientOnline.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ClientOnline.Repositorio.Infra
{
    public class Contexto : DbContext
    {
        public Contexto() : base("dbCliente") { }

        public DbSet<Cliente> Cliente { get; set; }


    }
}