﻿using ClientOnline.Repositorio.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ClientOnline.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            using (ClienteRepositorio repositorio = new ClienteRepositorio())
            {
                var lst = repositorio.Listar();
                Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(new { retorno = lst }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}